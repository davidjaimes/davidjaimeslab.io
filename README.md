```bash
.
├── _data
│   ├── main.yml
│   └── nav.yml
├── _includes
│   ├── footer.html
│   ├── head.html
│   ├── header.html
│   ├── main.html
│   ├── nav.html
│   └── section.html
├── _layouts
│   └── default.html
├── _pages
│   └── index.md
├── _sass
│   ├── body.scss
│   ├── footer.scss
│   ├── header.scss
│   ├── main.scss
│   ├── mobile.scss
│   ├── nav.scss
│   └── section.scss
├── assets
│   ├── css
│   │   └── styles.scss
│   └── images
│       ├── david-horizontal.jpeg
│       ├── david.jpg
│       ├── favicon.ico
│       └── monogram.png
├── Gemfile
├── LICENSE
├── README.md
└── _config.yml

8 directories, 26 files
```